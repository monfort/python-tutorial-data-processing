# Python Tutorial data processing

These files are materials of a Python course, from scratch to scikit-learn

It includes  :
- a collection of 20+ open source notebooks with many exercises.
- all the data you need to run the examples

Note that, because of obscure pedagogical reasons, the anwsers of the exercises are not avaiable.
