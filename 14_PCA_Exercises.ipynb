{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Principal component analysis in Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Reminder\n",
    "Principal component analysis (PCA) is a technique used in \n",
    "- approximation,\n",
    "- signal processing (noise reduction, compression…) \n",
    "\n",
    "but which can also lead to useful results in data analysis with a few aims :\n",
    "- visualisation and data exploration\n",
    "- dimensionality reduction\n",
    "- pattern classification, more specifically in unsupervised clustering"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Mathematical basis\n",
    "The basis of the PCA is to find the linear representation of the data that minimise the quadratic error while using a limited number of projection.\n",
    "\n",
    "We assume that we have a set of multivariate datapoints $(\\mathbf{x}_{i})_{i \\in [[1, m]]}$ living in $\\mathbb{R}^{n}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "### Order 0 approximation\n",
    "The best _single point_ to represent it in term of quadratic error is its mean.\n",
    "\n",
    "That is the point $\\mathbf{m}$ that minimise $\\sum_{i=1}^{m} ||\\mathbf{m} - \\mathbf{x}_i||^2$ is the empirical mean of the distribution : $\\mathbf{m} = \\frac{1}{m}\\sum_{i=1}^{m}\\mathbf{x}_i$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Order 1, 1D approximation\n",
    "We try to perform a linear (order 1) approximation of the data in 1D (that is, approximating by a monovariate dataset). In other word we try to find the vector $\\mathbf{e}$ ($||\\mathbf{e}||=1$) which minimise the quadratic error in projection :\n",
    "\n",
    "$$ \\underset{||\\mathbf{e}||=1}{\\operatorname{argmin}}\\left(\\sum_{i=1}^{m}|| \\mathbf{x}_i - (\\mathbf{m} + a_i\\mathbf{e})||^2 \\right)$$\n",
    "\n",
    "You have seen that the min of this quadratic error is attained for :\n",
    "$$a_i = <(\\mathbf{x}_i - \\mathbf{m}),\\mathbf{e}> $$\n",
    "and most interestingly that $\\mathbf{e}$ is an eigenvector attached to the largest eigenvalue of the scatter matrix of the dataset : \n",
    "$$\\mathbf{S} = \\sum_{i=1}^{m}(\\mathbf{x}_i - \\mathbf{m})(\\mathbf{x}_i - \\mathbf{m})^t$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Order 1, kD approximation\n",
    "We want now to minimise the quadratic error of a k-dimensional linear approximation of the dataset :\n",
    "\n",
    "$$ \\sum_{i=1}^{m}|| \\mathbf{x}_i - (\\mathbf{m} + \\sum_{j=1}^{k}a_{i,j}\\mathbf{e}_j)||^2 $$ \n",
    "\n",
    "\n",
    "We can extend the result of the 1D linear approximation. This leads to project the dataset on the (partial) basis made of the $k$ eigenvector corresponding to the $k$ largest eigenvalues of the dataset's scatter matrix $\\mathbf{S}$.\n",
    "\n",
    "As $\\mathbf{S}$ is a positive semi-definite matrix, the eigenvectors form a orthogonal basis. Hence the $(\\mathbf{e}_j)_{j\\in[[1, k]]}$ form a (partial) orthonormal basis of $\\mathbb{R}^n$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Principal component analysis\n",
    "\n",
    "Performing principal component analysis is all about computing the scatter matrix from the dataset and then performing the eigenvector decomposition of this matrix.\n",
    "\n",
    "If $\\mathbf{X}_\\mathsf{c}$ is the matrix with row $i$ containing $(\\mathbf{x}_i - \\mathbf{m})$, that is the _centered data_, then one can compute $\\mathbf{S} = \\mathbf{X}_\\mathsf{c}^t\\mathbf{X}_\\mathsf{c}$ as a simple outer product."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "We still have to compute the eigenvector decomposition once $\\mathbf{S}$ is computed :\n",
    "\n",
    "$$ \\mathbf{S} = \\mathbf{P}^t \\mathbf{D} \\mathbf{P}$$\n",
    "\n",
    "With $\\mathbf{P}$ being a $n \\times n$ *orthogonal matrix*, that is $\\mathbf{P}^{-1} = \\mathbf{P}^t$. In other words a matrix corresponding to the change into another orthonormal basis (considering the initial basis is itself orthonormal)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Isotropic or anisotropic data / homogenuos or heterogenuous dimensions\n",
    "Doing a PCA is essentially performing a rotation of the basis (changing from a orthonormal basis to another one) such that the scatter matrix, $\\mathbf{S}$ becomes diagonal.\n",
    "\n",
    "This rotation makes senses only if the dataset space is isotropic (we can consider that the canonical basis is already orthonormal), or in other words that the dataset space is made of homogenuous dimensions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Otherwise the _rotation_ might have no sense since it require performing linear combination of value which are in different unit and even worse **_dimension_** (mixing **length** with **mass**, or the like)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Quadratic error / isotropic loss\n",
    "Another way to understand the same question : PCA objective is to minimise **quadratic error**. The main specificity (and advantage) of **quadratic error** is that it is **invariant by rotation** of the basis.\n",
    "\n",
    "In other words, one would consider **quadratic error** mostly to benefit from an **isotropic loss function**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## The _iris_ dataset example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Reading in the data : 150 samples of 3 different species of _iris_, each sample caracterised by 4 length (_in cm_)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "## Getting the dataset :\n",
    "import pandas\n",
    "import numpy\n",
    "\n",
    "iris = pandas.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data',\n",
    "                       header=None,\n",
    "                       names=['sepal_len', 'sepal_wid', 'petal_len', 'petal_wid', 'species'],\n",
    "                       sep=',')\n",
    "## A bit of displaying :\n",
    "print(iris.head())\n",
    "print(iris.tail())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "notice that each variable is a length, in _cm_, hence we are in the case that all variables are of the same _physical dimension_, and _unit_. The last variable (_column_) is a (_dimension less_) classification label."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "## Isolating the variables (columns 0:4) and computing the scatter matrix\n",
    "X = iris.iloc[:, 0:4].values\n",
    "## From the classification of each sample \n",
    "cl = iris.iloc[:, 4].values\n",
    "\n",
    "print(numpy.unique(cl))\n",
    "print(iris.columns.values)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## Some visualisation\n",
    "import numpy\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as pyplot\n",
    "%matplotlib inline\n",
    "\n",
    "bins=numpy.linspace(X[::, 0].min(),X[::, 0].max(), 20)\n",
    "\n",
    "for c in numpy.unique(cl) :\n",
    "    pyplot.hist(X[cl==c, 0], bins, alpha=0.5, label=c)\n",
    "pyplot.legend(loc='upper right')\n",
    "pyplot.xlabel(iris.columns.values[0])\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 1 : \n",
    "Do the same figure for all 4 variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## Doing the same figure, for all 4 variables :\n",
    "fig, axes = pyplot.subplots(nrows=1, ncols=4, figsize=[15,4])\n",
    "\n",
    "for col in range(0, 4):\n",
    "    ######### COMPLETE THESE LINES in Displaying the histogram of the variable"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Complete tis code**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## Computing the _centered_ scatter matrix :\n",
    "means =            ######### COMPLETE this LINE vector of means of variable\n",
    "print(\"mean values :\", means)\n",
    "S =                ######### COMPLETE this line Centered scatter matrix\n",
    "print(\"Scatter matrix:\")\n",
    "print(S)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## Performing the spectral decomposition\n",
    "import numpy.linalg\n",
    "pca_vals, pca_vects = numpy.linalg.eig(S)\n",
    "print(\"PCA values :\", pca_vals)\n",
    "print(\"PCA directions :\\n\", pca_vects)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Scatter matrix _vs._ covariance matrix\n",
    "Indeed if we note $\\Sigma$ the empirical coviarance matrix of the data set we then have\n",
    "\n",
    "$$ \\Sigma = \\frac{1}{n-1}\\mathbf{S} $$\n",
    "\n",
    "Since the eigenvector decomposition of a matrix is scale invariant (only the eigenvalues are affected by the scale), one can perform this decomposition on the covariance matrix to get the PCA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "Sigma = numpy.cov(X.T) ## This «also» performs the data centering\n",
    "pca_vals, pca_vects = numpy.linalg.eig(Sigma)\n",
    "print(\"PCA values (cov):\", pca_vals)\n",
    "print(\"PCA values (scatter):\", pca_vals * (X.shape[0]-1)) ## barely rescaling the eigenvalues\n",
    "print(\"PCA directions :\\n\", pca_vects)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Projection(s) on the PCA\n",
    "Performing the PCA of the dataset corresponds to finding an _adapted_ orthonormal basis for the dataset : _adapted_ here means that the basis is such that the scatter matrix of the dataset (in this basis) is diagonal.\n",
    "\n",
    "Once this (_partial_-)basis is found, the most natural thing is to apply this change of basis to the dataset. Since the (partial-)basis is orthonormal, this can simply be done by projecting the data on found vectors.\n",
    "\n",
    "**NB** : this indeed make sense on the _centered data_, not the raw dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## Computing the projection using a «simple» matrix product\n",
    "X_dec = (X - means) @ pca_vects\n",
    "print(X_dec[0:10, ::])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 2 : \n",
    "Plot the histograms of the PCA values (4 axis)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "### Histograms on projections :\n",
    "fig, axes = pyplot.subplots(nrows=1, ncols=4, figsize=[15,4])\n",
    "\n",
    "for col in range(0, 4):\n",
    "    bins=numpy.linspace(X_dec[::, col].min(),X_dec[::, col].max(), 20)\n",
    "    ## Histograms of the PCA values (4 axis)\n",
    "    ######### COMPLETE THESE LINES #########"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "### axis pairwise scatter plot\n",
    "numpy.array(pandas.factorize(iris['species']))\n",
    "\n",
    "pyplot.scatter(X_dec[::, 0], X_dec[::, 1], c=pandas.factorize(iris['species'])[0])\n",
    "pyplot.xlabel(\"PCA axis 1\")\n",
    "pyplot.ylabel(\"PCA axis 2\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## A bit better printout :\n",
    "pyplot.figure(figsize=[10,7])\n",
    "\n",
    "for c in numpy.unique(cl) :\n",
    "    pyplot.scatter(X_dec[cl==c, 0], X_dec[cl==c, 1], label=c, alpha=0.5)\n",
    "pyplot.legend(loc='upper right')\n",
    "pyplot.xlabel(\"PCA axis 1\")\n",
    "pyplot.ylabel(\"PCA axis 2\")\n",
    "\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Shortcut\n",
    "\n",
    "Indeed the ```scikit``` python module includes a direct implementation of the PCA (not in kit) : ```sklearn.decomposition.PCA```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "import sklearn.decomposition\n",
    "\n",
    "iris_pca = sklearn.decomposition.PCA()\n",
    "iris_proj = iris_pca.fit_transform(X) ## X : data from the iris dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## A bit better printout :\n",
    "pyplot.figure(figsize=[10,7])\n",
    "\n",
    "for c in numpy.unique(cl) : ## SKlearn version\n",
    "    pyplot.scatter(iris_proj[cl==c, 0], iris_proj[cl==c, 1], label=c, alpha=0.5)\n",
    "for c in numpy.unique(cl) : ## «kit» (computing S, then diagonalisation…) version\n",
    "    pyplot.scatter(X_dec[cl==c, 0], -X_dec[cl==c, 1], label=c, alpha=1, marker='.')\n",
    "pyplot.legend(loc='upper right')\n",
    "pyplot.xlabel(\"PCA axis 1\")\n",
    "pyplot.ylabel(\"PCA axis 2\")\n",
    "\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "### Printing components as column vectors\n",
    "print(\"PCA directions :\\n\", pca_vects)\n",
    "print(\"Using SK-learn version :\\n\", iris_pca.components_.T)\n",
    "## To see it in the same order"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Notice** that this is merely the projection of the dataset to a plan minimising the quadratic error (to the _full, 4D_ dataset).\n",
    "\n",
    "One could as well produce the projection on any other basis. Particular projections are the one on the natural basis of the dataset _ie_ on two of the variables observed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 3\n",
    "Display Petal length vs. Petal width\n",
    "\n",
    "Use different color for the different classes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## A bit better printout :\n",
    "pyplot.figure(figsize=[10,7])\n",
    "\n",
    "## Display Petal length vs. Petal width\n",
    "###\n",
    "\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Not centering data\n",
    "\n",
    "The _centered PCA_ decomposition corresponds to the diagonalisation of the centered scatter matrix; and as such is equivalent to the diagonalisation of the covariance matrix.\n",
    "\n",
    "This decomposition corresponds to a rotation, but also a translation from origin to the mean point of the dataset.\n",
    "\n",
    "Sometimes (**rarely**) one might be interested in a rotation alone (without any translation). That is solving :\n",
    "\n",
    "$$ \\underset{||\\mathbf{e_j}||=1, j\\in[[1, k]]}{\\operatorname{argmin}}\\left(\\sum_{i=1}^{m}|| \\mathbf{x}_i - \\sum_{j=1}^{k}a_{i,j}\\mathbf{e}_j||^2\\right) $$\n",
    "\n",
    "This correspond to perform 1st order approximation inforcing null intercept."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "S_noC = X.T @ X\n",
    "print(\"Non-centered scatter matrix:\")\n",
    "print(S_noC)\n",
    "\n",
    "## Performing the spectral decomposition\n",
    "import numpy.linalg\n",
    "pca_vals_noC, pca_vects_noC = numpy.linalg.eig(S_noC)\n",
    "print(\"Non-centered PCA values :\", pca_vals_noC)\n",
    "print(\"Non-centered PCA directions :\\n\", pca_vects_noC)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Again we can display the projected data on the plan based on the principal components of higher values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "## Computing the projection using a «simple» matrix product\n",
    "X_dec_noC = X @ pca_vects_noC\n",
    "print(X_dec_noC[0:10, ::])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## And display on first two PCA :\n",
    "pyplot.figure(figsize=[10,7])\n",
    "\n",
    "for c in numpy.unique(cl) :\n",
    "    pyplot.scatter(X_dec_noC[cl==c, 0], X_dec_noC[cl==c, 1], label=c, alpha=0.5)\n",
    "pyplot.legend(loc='upper right')\n",
    "pyplot.xlabel(\"(non-centered) PCA axis 1\")\n",
    "pyplot.ylabel(\"(non-centered)PCA axis 2\")\n",
    "\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Notice** how in this case the projected values are not centered on the origin (as the data itself is not centered)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "print(\"For centered projection :\\n\", numpy.mean(X_dec, axis=0))\n",
    "print(\"For NON-centered projection :\\n\", numpy.mean(X_dec_noC, axis=0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Indeed the *non-centered* first component is often close the direction of the mean"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 4\n",
    "Complete the code bellow"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "### COMPLETE THESE LINES\n",
    "means =                       \n",
    "means_u =                      ### unit vector corresponding to means\n",
    "print(\"Normalised mean vector of the dataset :\\n\", ???)\n",
    "print(\"First component of NON-centered PCA :\\n\", ???)\n",
    "print(\"Angle between them : \", ???, \"rad\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Solving the anisotropic case\n",
    "When the native data, hence the _centered_ data, is not homogenuous a solution is to bring it to homogeneity in dimension by transforming each variable into a _dimension less_ value. This is typically done by dividing a variable by a value of the exact same _physical dimension_ (and unit). This produce _dimension less_ variables that express the characteristics of each datapoint relatively to a _scale_ that is _arbitrary_ for each variable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "## Normalised/standardised data\n",
    "A standard way to find a _scale_ that can be applied to each variable is to take, for each variable, the _scale_ to be the **standard deviation** of this variable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Weighting\n",
    "Transforming each variable to a _dimension less_ variable also corresponds to setting a weigth on each variable such that they are all _in fine_ on a **common** _relative_ dimension. In this contexte, **normalisation/standardisatin** correspond to giving the same weight to every variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import pandas\n",
    "import numpy\n",
    "\n",
    "USArrests = pandas.read_csv('USArrests.csv', header=0, sep=\";\", decimal=\",\")\n",
    "USArrests.head(5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## Isolating the variables (columns 0:4) and computing the scatter matrix\n",
    "Vars = USArrests.iloc[:, 1:5].values\n",
    "## From the classification of each sample \n",
    "states = USArrests.iloc[:, 0].values\n",
    "\n",
    "print(numpy.unique(states))\n",
    "print(USArrests.columns.values)\n",
    "print(Vars[:5,])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import matplotlib\n",
    "import matplotlib.pyplot as pyplot\n",
    "%matplotlib inline\n",
    "\n",
    "fig, axes = pyplot.subplots(nrows=1, ncols=4, figsize=[15,4])\n",
    "\n",
    "for col in range(0, 4):\n",
    "    axes[col].hist(Vars[::, col], alpha=0.5)\n",
    "    axes[col].set_xlabel(USArrests.columns.values[col+1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 5 : \n",
    "Compute the _centered_ scatter matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## Computing the _centered_ scatter matrix :\n",
    "##### COMPLETE THESE LINES\n",
    "USA_means = \n",
    "print(\"mean values : \", USA_means)\n",
    "USA_S = \n",
    "print(\"Scatter matrix:\")\n",
    "print(USA_S)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### First not caring about _heterogeneity_ of dimensions\n",
    "No _standardisation_ (or _scaling_) of the data is performed before applying PCA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 6 : \n",
    "Perform the spectral decomposition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "## Performing the spectral decomposition\n",
    "import numpy.linalg\n",
    "##### COMPLETE THIS LINE\n",
    "???\n",
    "print(\"PCA values :\", pca_vals_no_scale)\n",
    "print(\"PCA directions :\\n\", pca_vects_no_scale)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "Vars_dec_no_scale = (Vars - USA_means) @ pca_vects_no_scale\n",
    "print(Vars_dec_no_scale[0:10, ::])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pyplot.figure(figsize=[10, 7])\n",
    "##### COMPLETE THESE LINES\n",
    "### Display ...\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Performing some scaling\n",
    "The idea is to try to get all the initial variable on a joint _scale_. This may be acheived by deviding each variable by its standard deviation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 7 : \n",
    "Complete the code bellow\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "USA_sds = ???\n",
    "print(\"SD values : \", USA_sds)\n",
    "USA_S_scaled = ???\n",
    "print(\"Scatter matrix: (standardised)\")\n",
    "print(USA_S_scaled)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pca_vals_scaled, pca_vects_scaled = numpy.linalg.eig(USA_S_scaled)\n",
    "print(\"PCA values :\", pca_vals_scaled)\n",
    "print(\"PCA directions :\\n\", pca_vects_scaled)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "Vars_dec_scaled = ???\n",
    "print(Vars_dec_scaled[0:10, ::])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pyplot.figure(figsize=[10, 7])\n",
    "pyplot.scatter(Vars_dec_scaled[::, 0], Vars_dec_scaled[::, 1])\n",
    "pyplot.xlabel(\"PCA axis 1\")\n",
    "pyplot.ylabel(\"PCA axis 2\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Shortcut using sklearn\n",
    "\n",
    "As for non-reduced/non-standardized data, `sklearn` can perform centered and reduced PCA in using `sklearn.decomposition.PCA`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## Non reduced PCA\n",
    "import sklearn.decomposition\n",
    "\n",
    "usaa_proj = sklearn.decomposition.PCA().fit_transform(Vars)\n",
    "\n",
    "pyplot.figure(figsize=[10,7])\n",
    "pyplot.scatter(usaa_proj[:, 0], usaa_proj[:, 1])\n",
    "pyplot.xlabel(\"PCA axis 1\")\n",
    "pyplot.ylabel(\"PCA axis 2\")\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "## REDUCED PCA\n",
    "import sklearn.decomposition\n",
    "import sklearn.preprocessing\n",
    "## computing : ((Vars - USA_means) / USA_sds)\n",
    "usaa_cent_red = sklearn.preprocessing.StandardScaler().fit_transform(Vars)\n",
    "## getting the PCA :\n",
    "usaa_proj_sc = sklearn.decomposition.PCA().fit_transform(usaa_cent_red)\n",
    "\n",
    "pyplot.figure(figsize=[10,7])\n",
    "pyplot.scatter(usaa_proj_sc[:, 0], usaa_proj_sc[:, 1])\n",
    "pyplot.xlabel(\"PCA axis 1\")\n",
    "pyplot.ylabel(\"PCA axis 2\")\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### sklearn way :\n",
    "\n",
    "**NOTE** : There is a better *sklearn way* to perform this type of processing, using so calle `pipeline`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 8 : \n",
    "Complete the code bellow\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import matplotlib\n",
    "import matplotlib.pyplot as pyplot\n",
    "%matplotlib inline\n",
    "\n",
    "from sklearn.pipeline import make_pipeline\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "from sklearn.decomposition import PCA\n",
    "\n",
    "cent_PCA = ??? ## Using make_pipeline\n",
    "redu_PCA = ??? \n",
    "\n",
    "cent_usaa_proj = cent_PCA.fit_transform(Vars)\n",
    "redu_usaa_proj = redu_PCA.fit_transform(Vars)\n",
    "\n",
    "pyplot.figure(figsize=[10,7])\n",
    "pyplot.scatter(cent_usaa_proj[:, 0], cent_usaa_proj[:, 1])\n",
    "pyplot.xlabel(\"PCA axis 1\")\n",
    "pyplot.ylabel(\"PCA axis 2\")\n",
    "pyplot.show()\n",
    "\n",
    "pyplot.figure(figsize=[10,7])\n",
    "pyplot.scatter(redu_usaa_proj[:, 0], redu_usaa_proj[:, 1])\n",
    "pyplot.xlabel(\"PCA axis 1\")\n",
    "pyplot.ylabel(\"PCA axis 2\")\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## On effect of scaling (and importance to use it or not) see :\n",
    "\n",
    "https://scikit-learn.org/stable/auto_examples/preprocessing/plot_scaling_importance.html#sphx-glr-auto-examples-preprocessing-plot-scaling-importance-py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Copyright\n",
    "Copyright (C)  2017-2020  Serge Cohen, Bertrand Monfort, Frédéric-Emmanuel Picca under GDFL <a class=\"anchor\" id=\"GFDL\"></a>\n",
    "\n",
    "Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation;   \n",
    "with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.\n",
    "\n",
    "A copy of the license is included in the section entitled \"[GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3-standalone.html)\"."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
